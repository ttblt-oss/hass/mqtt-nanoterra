import sys

from setuptools import setup
from mqtt_nanoterra.__version__ import VERSION

if sys.version_info < (3,7):
    sys.exit('Sorry, Python < 3.7 is not supported')

install_requires = list(val.strip() for val in open('requirements.txt'))
tests_require = list(val.strip() for val in open('test_requirements.txt'))

setup(name='mqtt-nanoterra',
      version=VERSION,
      description=('Daemon managing Nano Terra '
                   'through MQTT'),
      author='Thibault Cohen',
      author_email='titilambert@gmail.com',
      url='http://gitlab.com/ttblt-hass/mqtt_nanoterra',
      package_data={'': ['LICENSE.txt']},
      include_package_data=True,
      packages=['mqtt_nanoterra'],
      entry_points={
          'console_scripts': [
              'nanoterra-mqtt = mqtt_nanoterra.__main__:nanoterra_mqtt',
              'nanoterra-cam = mqtt_nanoterra.__main__:nanoterra_cam'
          ]
      },
      license='Apache 2.0',
      install_requires=install_requires,
      tests_require=tests_require,
      classifiers=[
        'Programming Language :: Python :: 3.7',
      ]

)
