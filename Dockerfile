FROM arm32v7/python:3.7-slim-stretch
  
COPY requirements.txt /requirements.txt

RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install make gcc wget gnupg -y && \
    pip install pip --upgrade && \
    pip install -r /requirements.txt && \
    wget https://archive.raspbian.org/raspbian.public.key -O - | apt-key add - && \
    wget https://archive.raspberrypi.org/debian/pool/main/r/raspberrypi-firmware/libraspberrypi0_1.20200902-1_armhf.deb && \
    wget https://archive.raspberrypi.org/debian/pool/main/r/raspberrypi-firmware/raspberrypi-bootloader_1.20200902-1_armhf.deb && \
    dpkg -i libraspberrypi0_1.20200902-1_armhf.deb raspberrypi-bootloader_1.20200902-1_armhf.deb && \
    rm -f libraspberrypi0_1.20200902-1_armhf.deb && \
    apt-get purge make gcc wget gnupg -y && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt

RUN mkdir -p /app
WORKDIR /app

COPY Dockerfile /Dockerfile

COPY requirements.txt  setup.py  test_requirements.txt /app/
RUN pip install -r /app/requirements.txt
COPY mqtt_nanoterra /app/mqtt_nanoterra

RUN python setup.py install

ENV MQTT_USERNAME=username
ENV MQTT_PASSWORD=password
ENV MQTT_HOST=192.168.0.1
ENV MQTT_PORT=1883
ENV ROOT_TOPIC=homeassistant
ENV MQTT_NAME=mqtt-nanoterra
ENV LOG_LEVEL=INFO
